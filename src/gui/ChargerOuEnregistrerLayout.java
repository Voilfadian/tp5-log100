package gui;

import java.io.File;
import java.util.ArrayList;

import javax.swing.*;

import org.apache.commons.lang3.tuple.Pair;

import service.List;
import service.ReadWriteService;

public class ChargerOuEnregistrerLayout extends JPanel {

	private JButton buttonCharger;
	private JButton buttonEnregistrer;

	public ChargerOuEnregistrerLayout() {
		// Bouton charger le fichier
		buttonCharger = new JButton("Charger");
		buttonCharger.addActionListener(e-> chargerFichier());
		// Bouton enregistrer
		buttonEnregistrer = new JButton("Enregistrer");
		buttonEnregistrer.addActionListener(e -> sauvegarderFichier());

		add(buttonCharger);
		add(buttonEnregistrer);

		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
	}

	protected void sauvegarderFichier() {
		// pour sauvegarder le fichier
		JFileChooser saveFileChooser = new JFileChooser();
		saveFileChooser.setDialogTitle("Veuillez sp�cifier un fichier pour la sauvegarde");

		int userSelection = saveFileChooser.showSaveDialog(buttonCharger);

		if (userSelection == JFileChooser.APPROVE_OPTION) {
			File fileToSave = saveFileChooser.getSelectedFile();
			ReadWriteService.writeDictionary(fileToSave, List.getDictionnaire());
		}
	}
	
	protected void chargerFichier() {
		// pour charger le fichier
		JFileChooser saveFileChooser = new JFileChooser();
		ArrayList<Pair<String, String>> dictionnaire = new ArrayList<>();
		saveFileChooser.setDialogTitle("Veuillez sp�cifier un fichier � charger");

		JFileChooser readFileChooser = new JFileChooser();
		int result = readFileChooser.showOpenDialog(buttonEnregistrer);
		
		if (result == JFileChooser.APPROVE_OPTION) {
			File selectedFile = readFileChooser.getSelectedFile();
			dictionnaire = ReadWriteService.readDictionary(selectedFile);
			for(Pair<String, String> pair : dictionnaire) {
				List.ajouter(pair.getLeft(), pair.getRight());
			}
			RightLayout.refreshList();
		}
	}

}
