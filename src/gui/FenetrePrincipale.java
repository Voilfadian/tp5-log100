package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.*;

public class FenetrePrincipale {

	static JFrame frame;
	static JLabel definitionMot;

	protected static void afficherDefinition(String definition) {
		definitionMot.setText(definition);
	}

	public static void main(String args[]) {
		frame = new JFrame("Dictio");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 500);

		// Main layout
		// Layout Haut
		frame.add(new TopLayout(), BorderLayout.NORTH);

		// Layout Gauche
		LeftLayout leftLayout = new LeftLayout();
		leftLayout.setPreferredSize(new Dimension(200, 300));
		frame.add(leftLayout, BorderLayout.WEST);

		// Layout Millieu
		definitionMot = new JLabel();
		definitionMot.setText("test");
		frame.add(definitionMot, BorderLayout.CENTER);

		// Layout Droite
		RightLayout rightLayout = new RightLayout();
		rightLayout.setPreferredSize(new Dimension(200, 200));
		frame.add(rightLayout, BorderLayout.EAST);

		// Layout Bas
		// Bouton Ajouter / Modifier
		JButton buttonAjouterModifier = new JButton("Ajouter/Modifier");
		frame.add(buttonAjouterModifier, BorderLayout.SOUTH);

		frame.setVisible(true);
	}
}
