package gui;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;

import service.List;

public class LeftLayout extends JPanel {

	private JTextField recherche = new JTextField();
	private DefaultListModel<String> listeResultat;

	public LeftLayout() {
		// TextField pour �crire la recherche

		recherche.setPreferredSize(new Dimension(200, 50));
		recherche.addKeyListener(new PartirRecherche());

		// Label texte, pour afficher les r�sultats de recherche
		listeResultat = new DefaultListModel<>();
		listeResultat.addElement("");
		JList<String> listeResultatAffiches = new JList<>(listeResultat);
		listeResultatAffiches.setPreferredSize(new Dimension(200, 200));

		add(recherche);
		add(listeResultatAffiches);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		MouseListener mouseListener = new MouseAdapter() {
		    public void mouseClicked(MouseEvent e) {
		           String motSelection = (String) listeResultatAffiches.getSelectedValue();
		           String motARecherche = motSelection.substring(0, 1).toUpperCase() + motSelection.substring(1);
		           //Faire recherche d�finition et l'afficher
		           FenetrePrincipale.afficherDefinition(List.afficherDefinition(motARecherche));
		           
		    }
		};
		listeResultatAffiches.addMouseListener(mouseListener);
	}

	class PartirRecherche implements KeyListener {
		public void keyTyped(KeyEvent e) {
		}

		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				String resultatRecherche = recherche.getText().substring(0, 1).toUpperCase() + recherche.getText().substring(1);
				ArrayList<String> liste = List.afficherMot(resultatRecherche);
				listeResultat.clear();
				for(String s : liste) {
					listeResultat.addElement(s);
				}
			}
		}

		@Override
		public void keyReleased(KeyEvent arg0) {
		}
	}
	
	
}
