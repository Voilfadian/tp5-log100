package gui;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import service.List;

public class RightLayout extends JPanel {
	
	private static DefaultListModel<String> listeResultat;
	
	public RightLayout() {
		listeResultat = new DefaultListModel<>();
		
		listeResultat.addElement("test");
		
		JList<String> listeResultatAffiches = new JList<>(listeResultat);
		listeResultatAffiches.setPreferredSize(new Dimension(150, 300));
		JScrollPane scrollList = new JScrollPane(listeResultatAffiches);
		add(scrollList);
		MouseListener mouseListener = new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				String motSelection = (String) listeResultatAffiches.getSelectedValue();
				String motARecherche = motSelection.substring(0, 1).toUpperCase() + motSelection.substring(1);
				// Faire recherche définition et l'afficher
				System.out.println(motARecherche);
				System.out.println(List.afficherDefinition(motARecherche));
				FenetrePrincipale.afficherDefinition(List.afficherDefinition(motARecherche));

			}
		};
		listeResultatAffiches.addMouseListener(mouseListener);
		
	}
	
	protected static void refreshList() {
		listeResultat.clear();
		for(String s : List.afficher()) {
			listeResultat.addElement(s);
		}
	}
	
}
