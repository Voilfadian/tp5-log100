package gui;

import javax.swing.*;

public class TopLayout extends JPanel {

	public TopLayout() {
		JLabel texte = new JLabel();
	    texte.setText("Dictio");
	    
	    add(texte);
	    add(new ChargerOuEnregistrerLayout());
	    
	    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	}

}
