package service;

import java.util.*;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;


/**
	Cette classe cr�e une liste contenant des LexiNode
	@author Charles Bourgeois
	@version 1.0
	@since 2020/10/14
*/
public class List<T> {
	
	public static Vector<LexiNode> tete = new Vector<LexiNode>();
	public static LexiNode lastElement;
	private static ArrayList<String> ListeMot = new ArrayList<String>();
	
	/**
		Cette m�thode trouve la position de la lettre dans la tete
		Complexit� : O(n)
		@requires lettre != null
		@param tete : la t�te
		@param lettre : la lettre � comparer
		@return la position de la lettre dans la t�te
	*/
	public static int findPosition(Vector<LexiNode> tete, char lettre) {
		for(int i= 0; i < tete.size(); i++)
			if(tete.get(i).lettre == lettre)
				return i;
		 return -1;
	}
	
	/**
		Cette m�thode efface le contenu de la liste de mot
		Complexit� : O(k)
	 */
	public static void clearListeMot(){
		tete.clear();
    }
	
	/**
		Cette classe cr�e un noeud qui contient une lettre repr�sentante,
		un mot courant form� de la racine de l�arbre jusqu�� la lettre repr�sentante,
		une d�finition (vide si le mot courant n�est pas d�fini) et une
        collection de r�f�rences vers ses enfants.
		@author Charles Bourgeois
		@version 1.0
		@since 2020/10/14
	 */
	public static class LexiNode{
		public Vector<LexiNode> enfant = new Vector<LexiNode>();
        public char lettre;
        public String mot;
        public String definition;
        public LexiNode (char aLettre){
            this.lettre = aLettre;
            this.mot = String.valueOf(aLettre);
        }
        public LexiNode(char aLettre, String mot){
            this.lettre = aLettre;
            this.mot = mot;
        }
        public LexiNode(char aLettre, String mot, String def){
            this.lettre = aLettre;
            this.mot = mot;
            this.definition = def;
        }
    }
	
	/**
		Cette m�thode appele la m�thode pour modifier une d�finition r�cursivement
		Complexit� : O(K)
		@requires mot != null
		@param mot : le mot � changer sa d�finition
		@param nouvelleDef : la nouvelle d�finition
	*/
	public static void modifier(String mot, String nouvelleDef) {
		modifierRecursif(tete.get(findPosition(tete, mot.charAt(0))).enfant,mot,nouvelleDef); 
	}
	
	/**
		Cette m�thode s'appelle r�cursivement pour trouver le mot envoyer en param�tre
		et modifie la d�finition lorsqu'il le trouve.
		Complexit� : O(n*log(n))
		@requires mot != null
		@param enfant : noeud envoy�
		@param mot : le mot
		@param nouvelleDef : la nouvelle d�finition
	 */
	 public static void modifierRecursif(Vector<LexiNode> enfant, String mot, String nouvelleDef) {
	    	for(LexiNode p : enfant)
	        {
	    		if(!p.mot.equals(".")) {
	    			lastElement = p;
	    			modifierRecursif(p.enfant, mot, nouvelleDef);
	    		}
	    		else if(lastElement.mot.equals(mot)) 
	    				p.definition = nouvelleDef;
	        } 	
	 }
	 
	 /**
		Cette m�thode ajoute une tete si elle n'existe pas
		et appelle ajouterRecursif pour ajouter les autres noeuds
		Complexit� : O(n)
		@requires mot != null
		@param mot : le mot � ajouter
		@param def : sa d�finition
	*/ 
	public static void ajouter(String mot, String def){
		Vector<LexiNode> current = tete;
		for (int i = 0 ; i != mot.length() ; i++) {
		    char lettre = mot.charAt(i);
		    if(i==0) {
		    	lastElement = new LexiNode(lettre);
		    	int nb = findPosition(tete, lettre);
		    	if(nb != -1) 
		    		current = tete.get(nb).enfant; 
			    else {
			    	tete.add(lastElement);		
			    	current = tete.get(tete.size()-1).enfant;
			    }	    	
		    }
		    else 
		    	current = ajouterRecursif(lettre,current);		 		       		
		}	
		current.add(new LexiNode('.', "." ,def)); //fin du mot
    }
	
     /**
		Cette m�thode ajoute des noeuds si il n'exsite pas
		Complexit� : O(n)
		@param lettre : la lettre � ajouter
		@param noeud : noeud du parent
	 */
	 public static Vector<LexiNode> ajouterRecursif(char lettre, Vector<LexiNode> noeud){
		 
		 String parent = lastElement.mot;
		 
		 for (int i = 0 ; i != noeud.size() ; i++) {
			 if(noeud.get(i).lettre == lettre) {
				 lastElement = noeud.get(i);
				 return noeud.get(i).enfant;
			 }			 
		 }
		 
		 lastElement = new LexiNode(lettre,parent+lettre);
		 noeud.add(lastElement);
		 return noeud.get(noeud.size()-1).enfant;
	 }

	/**
		Cette m�thode appele de mani�re r�cursive la m�thode afficherRecursif
		Complexit� : O(n)
		@return ArrayList<String> de tous les mots
	*/ 
    public static ArrayList<String> afficher(){
    	ListeMot = new  ArrayList<String>();
    	for(LexiNode p : tete)
    		afficherRecursif(p.enfant, " ");
    	return ListeMot;
    }
    
    /**
		Cette m�thode appele de mani�re r�cursive la m�thode afficherRecursif
		Complexit� : O(n)
		@requires mot != null
		@param mot : le mot
		@return ArrayList<String> de tous les mots commencant par le mot donn� en param�tre
    */ 
    public static ArrayList<String> afficherMot(String mot){
    	ListeMot = new  ArrayList<String>();
    	
    	if(findPosition(tete, mot.charAt(0)) != -1)
    	return afficherRecursif(tete.get(findPosition(tete,mot.charAt(0))).enfant,mot); 	
    	
    	return ListeMot;
    }
    
    /**
		Cette m�thode appele de mani�re r�cursive la m�thode afficherDefinitionRecursif
		Complexit� : O(n)
		@requires mot != null
		@param mot : le mot
		@return la d�finition
    */ 
    public static String afficherDefinition(String mot){
    	if(findPosition(tete, mot.charAt(0)) != -1)
    	return afficherDefinitionRecursif(tete.get(findPosition(tete, mot.charAt(0))).enfant,mot); 
    	
    	return "";
    }
    
     /**
		Cette m�thode affiche la d�finition du mot donn� en param�tre
		Complexit� : O(n*log(n))
		@param enfant : enfant du noeud pr�c�dent 
		@param mot : le mot
		@return la d�finition
     */ 
    public static String afficherDefinitionRecursif(Vector<LexiNode> enfant, String mot) {
    	for(LexiNode p : enfant)
        {
    		if(!p.mot.equals(".")) {
    			lastElement = p;
    			return afficherDefinitionRecursif(p.enfant, mot);
    		}
    		else if(lastElement.mot.equals(mot)) 
    			return p.definition;
        }
    	return "";
    }
    
    
    /**
		Cette m�thode met dans une liste tous les mots qui contient le mot donn� en param�tre
		OU si le mot est = " ", il met dans une liste tous les mots
		Complexit� : O(n*log(n))
		@param enfant : enfant du noeud pr�c�dent
		@param mot : le mot
		@return ArrayList<String> liste contenant les mots
    */  
    public static ArrayList<String> afficherRecursif(Vector<LexiNode> enfant, String mot) {
    	for(LexiNode p : enfant)
        {
    		if(!p.mot.equals(".")) {
    			lastElement = p;
    			afficherRecursif(p.enfant, mot);
    		}
    		else if(!mot.equals(""))
    			if(lastElement.mot.contains(mot)) 
    				ListeMot.add(lastElement.mot);  		
    		else if(mot.equals(" "))
    			ListeMot.add(lastElement.mot);				
        }
    	return ListeMot;
    }
    
    
    /**
		Cette m�thode cr�e une liste de pair contenant tous les mots et de leur d�finition
		Complexit� : O(n)
		@return ArrayList<String> liste contenant les mots et de leur d�finition
     */ 
    public static ArrayList<Pair<String, String>> getDictionnaire() {
        ArrayList<Pair<String, String>> dictio = new ArrayList<>();
        for (String s : afficher()) {
            dictio.add(new ImmutablePair<String, String>(s, afficherDefinition(s)));
        }
        return dictio;
    }
       
    public static void main(String args[]) {       
       
        List.ajouter("Bien", "�tre heureux");
        List.ajouter("Bienvenue", "�a");
        List.ajouter("Biens", "�a");
        List.ajouter("Baen", "�a");
        List.ajouter("Banane", "�a");
        List.ajouter("Biscuit", "�a");
        List.ajouter("Bouqin", "�a");
        List.ajouter("B�b�", "�a");
        List.ajouter("Boursoufl�", "�a");
        List.ajouter("Test", "�a");
        List.ajouter("Jus", "�a");                
        
        System.out.println(List.afficher());
        System.out.println(List.afficherMot("B")); 
        System.out.println(List.afficherDefinition("Bien"));   
        List.modifier("Bien", "allo");
        System.out.println(List.afficherDefinition("Bien")); 
    }
}
