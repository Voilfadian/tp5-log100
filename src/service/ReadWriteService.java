package service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

/**
 * Cette Classe est une classe de service pour �crire et lire de fichiers
 * @author Cedric
 * @version 1.0
 * @since 2020/10/14
 */
public class ReadWriteService {
	
	/**
	 * Cette m�thode sert � lire un dictionnaire �  partir d'un fichier
	 * @param file, le fichier dans lequel on lit les donn�es
	 * @return une liste de pair de String. La cl� correspond au mot et la valeur � la  d�finition
	 */
	public static ArrayList<Pair<String, String>> readDictionary(File file){
		List<String> lines = new ArrayList<String>();
		ArrayList<Pair<String, String>> dictionary = new ArrayList<>();
		/* Cette partie de code est fortement inspir�e du tutoriel suivant :
		 * https://www.javatpoint.com/how-to-read-file-line-by-line-in-java
		 * Le code est utilis� pour lire le fichier ligne par ligne et le mettre dans une string
		 */
		try {
			File f = new File(file.getAbsolutePath());
			BufferedReader br = new BufferedReader(new FileReader(f));
			String line;
			while((line = br.readLine())!=null) {
				lines.add(line);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}// Fin de la partie inspir�e
		
		for(String line : lines) {
			String[] entry = line.split("&");
			dictionary.add(new ImmutablePair<String, String>(entry[0].trim(), entry[1].trim()));
		}
		return dictionary;
	}
	
	/**
	 * Cette m�thode sert � �crire un dictionnaire dans un fichier
	 * @param file, le fichier dans lequel on veut �crire le dictionnaire courant
	 * @param dictionary, une liste de pair qui contient des String, la cl� correspond au mot et la valeur � la d�finition
	 */
	public static void writeDictionary(File file, List<Pair<String, String>> dictionary) {
		try {
			BufferedWriter br = new BufferedWriter(new FileWriter(file.getAbsoluteFile()));
			for (Pair<String, String> entry : dictionary) {
				br.write(entry.getKey() + " & " + entry.getValue() + "\n");
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
