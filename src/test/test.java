package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Vector;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import service.List;
import service.List.LexiNode;

class test {
	
	@AfterEach
    public void cleanup() {
        List.tete.clear();
    }

    @Test
    public void testAjouterMot() {
        //Arrange
        String mot = "Test";
        String def = "je suis un test";
        //Act
        List.ajouter(mot, def);
        //Assert
        assertEquals(List.getDictionnaire().get(0).getLeft(), mot);
        assertEquals(List.getDictionnaire().get(0).getRight(), def);
        assertNotNull(List.getDictionnaire().get(0));
        assertNotEquals(List.getDictionnaire().get(0), "banane");
    }
    
    @Test
    public void testGetDictionnaire() {
    	String mot = "Test";
        String def = "je suis un test";
        List.ajouter(mot, def);
        
        assertEquals(mot , List.getDictionnaire().get(0).getLeft());
        assertEquals(def , List.getDictionnaire().get(0).getRight());
    }
    
    @Test
    public void testFindPosition() {
    	String mot = "Test";
        String def = "je suis un test";
        List.ajouter(mot, def);
        
        assertEquals(0, List.findPosition(List.tete, 'T'));
    }
    
    @Test
    public void testModifier() {
    	String mot = "Test";
        String def = "je suis un test";
        List.ajouter(mot, def);
        String nouvelleDef = "je suis une nouvelle définition";
        List.modifier("Test", nouvelleDef);
        
        assertEquals("je suis une nouvelle définition", List.getDictionnaire().get(0).getRight());
    }
    
    @Test
    public void testAfficher() {
    	String mot = "Test";
        String def = "je suis un test";
        List.ajouter(mot, def);
        ArrayList<String> s = List.afficher();
        
        assertEquals(List.getDictionnaire().get(0).getLeft(), s.get(0));
    }
    
    @Test
    public void testAfficherMot() {
    	String mot = "Test";
        String def = "je suis un test";
        List.ajouter(mot, def);
        ArrayList<String> s = List.afficherMot(mot);
        
        assertEquals(List.getDictionnaire().get(0).getLeft(), s.get(0));
    }
    
    @Test
    public void testAfficherDefinition() {
    	String mot = "Test";
        String def = "je suis un test";
        List.ajouter(mot, def);
        
        assertEquals(List.getDictionnaire().get(0).getRight(), List.afficherDefinition(mot));
    }
    
}
